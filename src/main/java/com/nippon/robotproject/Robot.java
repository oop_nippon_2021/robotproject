/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.robotproject;

/**
 *
 * @author Nippon
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection =' ';
    
    public Robot(int x , int y ,int bx , int by , int N){
        this.x=x;
        this.y=y;
        this.bx=bx;
        this.by=by;
        this.N=N;
    }
    public boolean inMap(int x ,int y){
         if(x>=N || x<0 || y>=N || y<0){
                    return false;
                }
         return true;
    }
    public boolean walk(char direction){
        switch(direction){
            case 'N':
                //if(x>=N || x<0 || y-1>=N || y<0){
                    //return false;
               // }
                if(!inMap(x,y-1)){
                    printUnableMove();
                    return false;
                }
                y=y-1;
                break;
            case 'S':
               // if(x>=N || x<0 || y+1>=N || y<0){
                  //  return false;
               // } 
                if(!inMap(x,y+1)){
                    printUnableMove();
                    return false;
                }
                y=y+1;
                
                break;
            case 'E':
                 //if(x+1>=N || x<0 || y>=N || y<0){
                   // return false;
              //  }
                if(!inMap(x+1,y)){
                    printUnableMove();
                    return false;
                }
                x=x+1;
                break;
            case 'W':
                 //if(x-1>=N || x<0 || y>=N || y<0){
                    //return false;
             //   }
                 if(!inMap(x-1,y)){
                     printUnableMove();
                    return false;
                }
                x=x-1;
                break;
        }
        lastDirection = direction;
        if (isBomb()){
            printBomb();
        }
         
        return true;
    }
        public boolean walk(char direction , int step){
            for(int i =0 ; i<step ;i++)
                if(!walk(direction)){
                    return false;
                }
        return true;
    }

         public boolean walk(){
             return this.walk(lastDirection);
         
         }
         public boolean walk(int step){
             return this.walk (lastDirection , step);
         
         }
         
  
    public String toString(){
        return "Robot (" + this.x + ", " +this.y + ")" + " (" + Math.abs(x-bx) + " , " + Math.abs(y-by) + ")";
    }
    public void printUnableMove(){
        System.out.println("I can't move!!!!!!!!!!!!!!!!!");
    }
    public void printBomb(){
        System.out.println("Bomb Found!!!!!!!!!!!!!!!!!");
    }
    public boolean isBomb(){
        if(x==bx && y==by){
            return true;
        }
        return false;
    }
}
